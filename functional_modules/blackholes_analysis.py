#!/usr/bin/python
"""
Gets as input a blackholes json file and does the analysis for each event
"""

import json
import sqlite3
import numpy as np
import csv
import os

__author__ = "Arakadakis Konstantinos"
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = "Arakadakis Konstantinos"
__email__ = "arakadakiskonstantinos@gmail.com"
__status__ = "Development"


def str_table_to_int(table):
    for i in range(0,len(table)):
        table[i] = int(table[i])
    return table

def blackholes_analysis(input_json_file_path,db_path,output_csv_file_path ):
    header =['Hijacker','Possible blackholes','Blackhole == Hijacker','Blackhole in AS-path','Hijacker in AS-path','Blackhole is origin', 'Avg distance Blackhole to last hop','Avg distance Blackhole to origin']

    with open(output_csv_file_path+'/blackholes_analysis.csv', mode='w') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(header)


    with open(input_json_file_path+'/blackholes.json') as f:
        data = json.load(f)

    print len(data)
    blackholes = {}
    for event in data:
        blackholes_temp = set()
        updates = event['updates']
        for update in updates:
            community = update['communities']
            for i in range(len(community)-1,-1,-1):
                if int(community[i]['value']) == 666:
                    blackholes_temp.add(int(community[i]['asn']))
                    break
        blackholes[event['hash']] = list(blackholes_temp)

    conn = sqlite3.connect(db_path)
    c = conn.cursor()

    for event in data:
        res = c.execute('SELECT detected_asn FROM possible_hijacks WHERE hash="{}"'.format(event['hash']))
        hijacker = res.fetchall()[0][0]
        bl_is_eq_hijacker = []
        bl_is_eq_hijacker = []
        for k in blackholes[event['hash']]:
            bl_is_eq_hijacker.append( hijacker == k)
        
        sum_hijack = 0
        bl_is_origin = []

        bl_in_as_path=[]
        for y in blackholes[event['hash']]:
            bl_is_orgin_ =0
            sum_bl = 0
            for i in event['updates']:
                if  y ==  str_table_to_int(i['as-path'].split(' '))[-1]:
                    bl_is_orgin_ += 1
                if y in str_table_to_int(i['as-path'].split(' ')):
                    sum_bl += 1

            bl_in_as_path.append(sum_bl/float(len(event['updates'])))
            bl_is_origin.append(bl_is_orgin_/float(len(event['updates'])))

        for i in event['updates']:
            if hijacker in str_table_to_int(i['as-path'].split(' ')):
                sum_hijack += 1

        to_last_hop_ = []
        to_origin_ = []

        for j in blackholes[event['hash']]:
            updates_calc = 0
            to_last_hop = 0
            to_origin = 0
            for i in event['updates']:
                set_to_last_hop = set()
                set_to_origin = set()
                as_path = str_table_to_int(i['as-path'].split(' '))

                if j in as_path:
                    updates_calc += 1   
                    for y in range(0,len(as_path)):
                        if as_path[y] == j:
                            break
                        set_to_last_hop.add(as_path[y])

                    for y in range(len(as_path)-1,-1,-1):
                        if as_path[y] == j:
                            break
                        set_to_origin.add(as_path[y])

                    to_last_hop += len(set_to_last_hop)
                    to_origin += len(set_to_origin)

            if float(updates_calc)==0:
                to_last_hop_.append(-1)
                to_origin_.append(-1)
            else:
                to_last_hop_.append(to_last_hop/float(updates_calc))
                to_origin_.append(float(to_origin/float(updates_calc)))
        with open(output_csv_file_path+'/blackholes_analysis.csv', mode='a') as csv_file:
            writer = csv.writer(csv_file)
            writer.writerow([hijacker,blackholes[event['hash']], bl_is_eq_hijacker,bl_in_as_path ,sum_hijack/float(len(event['updates'])),bl_is_origin,to_last_hop_,to_origin_ ])

    c.close()
    conn.close()

if __name__ == '__main__':
    path = os.path.abspath(os.path.dirname(__file__))
    blackholes_analysis(path,path)