#!/usr/bin/python
"""Creates the bgpstream_have_updates.json and blackholes.json"""

import json

__author__ = "Arakadakis Konstantinos"
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = "Arakadakis Konstantinos"
__email__ = "arakadakiskonstantinos@gmail.com"
__status__ = "Development"


def create_datasets(in_file_path):
	with open(in_file_path+'/bgpstream_updates24.json') as f:
	    data = json.load(f)

	have_updates = []
	for i in data:
		if i['updates']:
			have_updates.append(i)


	blackholes = []
	for i in have_updates:
		find  = False
		for update in i['updates']:
			for community in update['communities']:
				if community['value'] == 666:
					blackholes.append(i)
					find = True
					break
			if find:
				break

	with open(in_file_path+'/bgpstream_have_updates24.json', 'w') as outf:
	    json.dump(have_updates, outf, indent=4)

	with open(in_file_path+'/blackholes.json', 'w') as outf:
	    json.dump(blackholes, outf, indent=4)


if __name__ == '__main__':
	create_datasets('/home/konstantinos/Desktop/BGP-stream_parser')