# BGPstream events analysis

>This study aims to check over the validity of the events that are published on bgpstream.com by BGPmon. The events are stored in a database and based on them, we conduct research analysis.

![BGPmon](misc/bgpmon.gif)

## Database & Json documentation

#### Possible hijacks

| Column   | Description |
| -------- | -------- |
| Hash   | Event ID |
| Country   | The country the event was triggered from   |
| Expected ASN   | The valid prefix owner   |
| Detected ASN   | The hijacker [ Detected origin ASN ]   |
| Starttime year-month-day-time   | The time the event was first detected   |
| Endtime year-month-day-time   | The time the event was mitigated   |
| Peers   | The numbers of peers that announced the event to the BGPmon monitors |
| Expected prefix   | The prefix the valid owner normally announces   |
| Detected prefix   | The prefix the hijacker announced  |
| Detected AS Path   | The AS Path of the "hijack" announcement |


#### BGP Leaks

| Column   | Description |
| -------- | -------- |
| Hash   | Event ID |
| Country   | The country the event was triggered from   |
| Expected ASN   | The valid prefix owner   |
| Detected ASN   | The leaker   |
| Starttime year-month-day-time   | The time the event was first detected   |
| Endtime year-month-day-time   | The time the event was mitigated   |
| Peers   | The numbers of peers that announced the event to the BGPmon monitors |
| Leaked prefix   | The prefix that was leaked   |
| Leaked AS Path   | The AS path of the corresponding announcement  |
| Leaked to   | The AS the leaker announced the prefix to and triggered the event |


#### Outages

| Column   | Description |
| -------- | -------- |
| Hash   | Event ID |
| Country   | The country the event was triggered from   |
| Outage ASN   | The corresponding AS where the outage happened |
| Starttime year-month-day-time   | The time the event was first detected   |
| Endtime year-month-day-time   | The time the event was mitigated   |
| Affected prefixes | The number the prefixes that were affected by the outage |


>The Json file contains all the events that the parser has found. Each json has the same fields as the corresponding 
database table. In contrary to the database, the json file contains duplicates. 

### Examples of Json entries


#### Possible hijack

```
{
    "starttime_day": 11, 
    "expected asn": 60781, 
    "endtime_time": null, 
    "event_type": "Possible Hijack", 
    "detected_aspath": [
        11170, 
        7018, 
        6939, 
        39855
    ], 
    "country": null, 
    "moredetail": "https://bgpstream.com/event/145711", 
    "starttime_year": 2018, 
    "endtime_year": null, 
    "peers": 196, 
    "endtime_month": null, 
    "starttime_month": 8, 
    "detected_prefix": "136.144.16.0/24", 
    "expected_prefix": "136.144.16.0/22", 
    "endtime_day": null, 
    "detected asn": 39855, 
    "starttime_time": "06:21:46"
}
```

#### BGP Leak

```
{
    "starttime_day": 11, 
    "expected asn": 132123, 
    "starttime_time": "03:39:10", 
    "endtime_time": null, 
    "event_type": "BGP Leak", 
    "detected_aspath": [
        135610, 
        63956, 
        703, 
        7473, 
        7473, 
        7473, 
        7473, 
        7473, 
        6461, 
        10026, 
        9498, 
        58601, 
        174, 
        132602, 
        58715, 
        63969, 
        132123
    ], 
    "country": null, 
    "moredetail": "https://bgpstream.com/event/145696", 
    "starttime_year": 2018, 
    "endtime_year": null, 
    "peers": 68, 
    "endtime_month": null, 
    "starttime_month": 8, 
    "leaked_prefix": "103.70.229.0/24", 
    "detected asn": 58601, 
    "endtime_day": null, 
    "leaked_to": 9498
}
```

#### Outage

```
{
    "starttime_day": 11, 
    "affected_prefixes": 36, 
    "endtime_time": null, 
    "event_type": "Outage", 
    "starttime_year": 2018, 
    "outage asn": 52995, 
    "moredetail": "https://bgpstream.com/event/145715", 
    "endtime_year": null, 
    "endtime_month": null, 
    "starttime_month": 8, 
    "country": null, 
    "endtime_day": null, 
    "starttime_time": "07:42:00"
}
```

## BGPstream events analysis


| Analysis   | Description |
| -------- | -------- |
| RPKI Analysis   | We want to examine which prefixes were protected by RPKI ROAs. For this purpose, we use RIPE NCC RPKI Validator, that offers a REST API and answers whether a prefix is protected by ROA or not. |
| Geographic correlation   | We examine which hijacks are practiced between ASes in the same country or region  |
| Cone Analysis   | We aim to to examine how the cone of an AS was affected by the hijack against its prefix(es)   |
| Blackholes Analysis   | We aim to identify the events that are related to blackholing. |
| Impact analysis (Peers)   | We want to examine the difference in the impact of the hijack (i.e., peers infected) as it seen by the CAIDA’s BGPStream tool (bgpstream.caida.org) and it is reported by BGPmon’s streaming service.   |
| BGP Leaks violation   | We aim to to examine the BGP Leaks and tell in which cases the internet routing policies were actually violated.|


## How to use

```
git clone https://gitlab.com/konstantinosarakadakis/BGPstream
```
```bash
cd BGPstream
```
#### Update the database

```
./update_db.py
```
#### Perform analysis on the collected events

```
./analysis.py
```


## Prerequisites

```
Python2
```
```
pyBGPstream [some modules require it]
```

## Feel free to read the reports of this study!

<a href ='misc/Blackhole_communities.pdf' ><img src='misc/1.png'></a>
<a href ='misc/BGPstream_events_analysis.pdf' ><img src='misc/2.png'></a>


## Author

**Arakadakis Konstantinos** – konstantinosarakadakis@gmail.com


## License

This project is licensed under the MIT License - see the [LICENSE](misc/LICENSE) file for details
