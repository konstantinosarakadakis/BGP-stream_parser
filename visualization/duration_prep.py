#!/usr/bin/env python
import sqlite3
import datetime
import os 

try:
    os.remove("duration.txt") 
except:
    pass

conn = sqlite3.connect('record.db')
c = conn.cursor()

res1 = c.execute('SELECT detected_prefix, starttime_year, starttime_month, starttime_day, starttime_time, detected_asn, expected_asn,endtime_year, endtime_month, endtime_day, endtime_time,peers,expected_prefix,hash FROM possible_hijacks WHERE endtime_year!="None"')
res1 = res1.fetchall()

c.close()
conn.close()

for hijack in res1:
    tt = hijack[4].split(":")
    t1 = hijack[10].split(":")
    detect = int(hijack[0].split('/')[1])
    expect = int(hijack[12].split('/')[1])

    start = datetime.datetime(hijack[1],hijack[2],hijack[3],int(tt[0]),int(tt[1]),int(tt[2]))
    end = datetime.datetime(hijack[7],hijack[8],hijack[9],int(t1[0]),int(t1[1]),int(t1[2]))
    duration =  int((end - start).total_seconds())
    if detect == expect:
        with open('duration.txt', 'a') as the_file:
            the_file.write(str(duration)+" 0\n")
    else:
        with open('duration.txt', 'a') as the_file:
            the_file.write(str(duration)+" 1\n")
