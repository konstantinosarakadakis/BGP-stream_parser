import sqlite3
import os
try:
	os.remove("scatter6.txt") 
except:
	pass

try:
	os.remove("scatter4.txt") 
except:
	pass

conn = sqlite3.connect('record.db')
c = conn.cursor()

res1 = c.execute('SELECT detected_prefix, expected_prefix FROM possible_hijacks')
res1 = res1.fetchall()

c.close()
conn.close()
dataset_expect = {}
dataset_detect = {}

for k in res1:
    detect = k[0].split('/')[1]
    expect = k[1].split('/')[1]
    print expect,detect
    if ':' in k[0] or ':' in k[1]:
        with open('scatter6.txt', 'a') as the_file:
            the_file.write(expect+" "+detect+"\n")
    else:
        with open('scatter4.txt', 'a') as the_file:
            the_file.write(expect+" "+detect+"\n")

    try:
    	print dataset_expect[int(expect)]
    	dataset_expect[int(expect)] += 1
    except:
    	dataset_expect[int(expect)] = 1

    try:
    	print dataset_detect[int(detect)]
    	dataset_detect[int(detect)] += 1
    except:
    	dataset_detect[int(detect)] = 1

print '\n\n'

print "Expect prefixes :"
for i in range(0,121):
	try:
		print "/"+str(i)+'\t:\t'+str(dataset_expect[i])
	except:
		print "/"+str(i)+'\t:\t'+str(0)

print '\n\n'
print "Detect prefixes :"

for i in range(0,121):
	try:
		print "/"+str(i)+'\t:\t'+str(dataset_detect[i])
	except:
		print "/"+str(i)+'\t:\t'+str(0)
